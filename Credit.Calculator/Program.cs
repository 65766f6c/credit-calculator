﻿using System;
using System.Globalization;
using System.Linq;

namespace Credit.Calculator
{
    internal class Program
    {
        private static readonly ConsoleWrapper ConsoleWrapper = new ConsoleWrapper();

        static void Main(string[] args)
        {
            try
            {
                while (true) Process();
            }
            catch (Exception e)
            {
                ConsoleWrapper.SpecialWrite(e.Message)
                    .WriteLine(e.StackTrace)
                    .WriteLine();
            }
        }

        private static void Process()
        {
            Console.WriteLine("Credit calculator v.1.0");

            ConsoleWrapper.SpecialWrite("Enter the annual interest rate:")
                .ReadDecimal(out var percent);

            ConsoleWrapper.SpecialWrite("Enter the initial credit amount:")
                .ReadDecimal(out var initialAmount);

            ConsoleWrapper.SpecialWrite("Press 'm' for the average credit months mode or 'p' for the monthly payment mode:")
                .ReadChar(out var input)
                .WriteLine();

            decimal monthlyPayment;

            switch (char.ToLower(input))
            {
                case 'm':
                    ConsoleWrapper.SpecialWrite("Enter the average credit months:")
                        .ReadDecimal(out var months);
                    monthlyPayment = Core.GetAnnuityPayment(percent, initialAmount, months);
                    break;

                case 'p':
                    ConsoleWrapper.SpecialWrite("Enter the monthly payment:")
                        .ReadDecimal(out monthlyPayment);
                    break;

                default:
                    throw new InvalidOperationException("Invalid input. You need to push the 'm' or 'p' button. Terminating...");
            }

            var creditDetails = Core.GetCreditDetails(percent, initialAmount, monthlyPayment);

            var initialAmountString = creditDetails.InitialAmount.ToStringInvariant();
            var percentString = creditDetails.Percent.ToStringInvariantLong();
            var monthlyPaymentString = creditDetails.MonthlyPayment.ToStringInvariant();

            ConsoleWrapper.SpecialWrite($"credit details: {{ initial amount: {initialAmountString}, percent: {percentString}, " +
                $"months: {creditDetails.Months.Count}, monthly payment: {monthlyPaymentString} }}")
                .WriteLine();

            foreach (var monthDetails in creditDetails.Months)
            {
                string percentsString = monthDetails.Payment.Percents.ToStringInvariant();
                string mainDebtString = monthDetails.Payment.MainDebt.ToStringInvariant();
                string totalString = monthDetails.Payment.Total.ToStringInvariant();
                string restString = monthDetails.Rest.ToStringInvariant();
                string initialRestString = monthDetails.InitialRest.ToStringInvariant();

                Console.WriteLine($"month: {{ number: {monthDetails.MonthNumber}, initial rest: {initialRestString}, payment: {{ " +
                    $"percents: {percentsString}, main debt: {mainDebtString}, total: {totalString} }}, rest: {restString} }}");
            }

            ConsoleWrapper.SpecialWrite($"total percents: {creditDetails.TotalPercents.ToStringInvariant()}")
                .WriteLine($"total payments: {creditDetails.TotalPayments.ToStringInvariant()}")
                .WriteLine()
                .ReadLine();
        }
    }
}
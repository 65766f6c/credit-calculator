﻿using System;
using System.Globalization;

namespace Credit.Calculator
{
    internal static class StringExtensions
    {
        public static decimal ToDecimal(this string input)
        {
            return Decimal.TryParse(input, out var result)
                ? result
                : Decimal.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out var invariantCultureResult)
                    ? invariantCultureResult
                    : throw new InvalidOperationException("Invalid input. Terminating...");
        }
    }
}
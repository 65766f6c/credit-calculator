﻿using System;

namespace Credit.Calculator
{
    internal class Core
    {
        public static decimal GetAnnuityPayment(decimal percent, decimal initialAmount, decimal months)
        {
            decimal percentsPerMonth = percent / 12m / 100m;
            decimal annuityRate = percentsPerMonth * (1m + percentsPerMonth).Power(months) / ((1m + percentsPerMonth).Power(months) - 1m);
            return annuityRate * initialAmount;
        }

        public static CreditDetails GetCreditDetails(decimal percent, decimal initialAmount, decimal mounthlyPayment)
        {
            var creditDetails = new CreditDetails(initialAmount, mounthlyPayment, percent);
            return GetCreditDetailsInternal(creditDetails);
        }

        private static CreditDetails GetCreditDetailsInternal(CreditDetails creditDetails, MonthDetails previousMonthDetails = null)
        {
            int monthNumber = (previousMonthDetails?.MonthNumber ?? 0) + 1;
            var initialRest = previousMonthDetails?.Rest ?? creditDetails.InitialAmount;
            var payment = new Payment(initialRest, creditDetails.Percent, creditDetails.MonthlyPayment);
            var rest = initialRest - payment.MainDebt;
            var monthDetails = new MonthDetails(monthNumber, initialRest, rest, payment);
            creditDetails.Months.Add(monthDetails);
            return rest > 0 && Math.Round(rest) != decimal.Zero
                ? GetCreditDetailsInternal(creditDetails, monthDetails) // go deeper
                : creditDetails;
        }
    }
}

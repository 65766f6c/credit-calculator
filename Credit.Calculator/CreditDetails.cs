﻿using System.Collections.Generic;
using System.Linq;

namespace Credit.Calculator
{
    internal class CreditDetails
    {
        public CreditDetails(decimal initialAmount, decimal monthlyPayment, decimal percent)
        {
            InitialAmount = initialAmount;
            MonthlyPayment = monthlyPayment;
            Percent = percent;
        }

        public ICollection<MonthDetails> Months { get; } = new List<MonthDetails>();

        public decimal TotalPercents => Months.Sum(s => s.Payment.Percents);
        public decimal TotalPayments => Months.Sum(s => s.Payment.Total);

        public decimal InitialAmount { get; }
        public decimal MonthlyPayment { get; }
        public decimal Percent { get; }
    }
}
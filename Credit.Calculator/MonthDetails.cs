﻿namespace Credit.Calculator
{
    internal class MonthDetails
    {
        public MonthDetails(int monthNumber, decimal initialRest, decimal rest, Payment payment)
        {
            MonthNumber = monthNumber;
            InitialRest = initialRest;
            Rest = rest;
            Payment = payment;
        }

        public int MonthNumber { get; }
        public decimal InitialRest { get; }
        public decimal Rest { get; }
        public Payment Payment { get; }
    }
}
﻿using System;
using System.Globalization;

namespace Credit.Calculator
{
    internal static class DecimalExtensions
    {
        public static string ToStringInvariant(this decimal initial)
        {
            return initial.ToString("0.##", CultureInfo.InvariantCulture);
        }

        public static string ToStringInvariantLong(this decimal initial)
        {
            return initial.ToString("0.########", CultureInfo.InvariantCulture);
        }

        public static decimal Power(this decimal initial, decimal power)
        {
            return (decimal)Math.Pow((double)initial, (double)power);
        }
    }
}
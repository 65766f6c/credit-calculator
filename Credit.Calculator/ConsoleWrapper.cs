﻿using System;
using System.Runtime.CompilerServices;

namespace Credit.Calculator
{
    internal class ConsoleWrapper
    {
        public ConsoleWrapper SpecialWrite(string input)
        {
            Console.WriteLine();
            Console.WriteLine(input);
            return this;
        }

        public ConsoleWrapper WriteLine(string input)
        {
            Console.WriteLine(input);
            return this;
        }

        public ConsoleWrapper WriteLine()
        {
            Console.WriteLine();
            return this;
        }

        public ConsoleWrapper ReadLine()
        {
            Console.ReadLine();
            return this;
        }

        public ConsoleWrapper ReadDecimal(out decimal result)
        {
            result = Console.ReadLine().ToDecimal();
            return this;
        }

        public ConsoleWrapper ReadChar(out char result)
        {
            result = Console.ReadKey().KeyChar;
            return this;
        }
    }
}
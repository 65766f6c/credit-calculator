﻿namespace Credit.Calculator
{
    internal class Payment
    {
        public Payment(decimal rest, decimal percent, decimal monthlyPayment)
        {
            Percents = rest * percent / 12m / 100m;

            MainDebt = rest + Percents > monthlyPayment 
                ? monthlyPayment - Percents 
                : rest;

            Total = MainDebt + Percents;
        }

        public decimal MainDebt { get; }
        public decimal Percents { get; }
        public decimal Total { get; }
    }
}